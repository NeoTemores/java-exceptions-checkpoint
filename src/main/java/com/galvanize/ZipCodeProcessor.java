package com.galvanize;

public class ZipCodeProcessor {

    // don't alter this code...
    private final Verifier verifier;

    public ZipCodeProcessor(Verifier verifier) {
        this.verifier = verifier;
    }

    // write your code below here...
    public String process(String zipcode) {

        StringBuilder msg = new StringBuilder();

        //catches the appropriate errors
        try {
            verifier.verify(zipcode);
            msg.append("Thank you!  Your package will arrive soon.");

        } catch (InvalidFormatException e) {
            msg.append("The zip code you entered was the wrong length.");
//            e.printStackTrace();

        } catch (NoServiceException e) {
            msg.append("We\'re sorry, but the zip code you entered is out of our range.");
//            e.printStackTrace();
        }

        return msg.toString();
    }


}
