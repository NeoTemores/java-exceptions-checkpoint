package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {
    Verifier verifier;
    ZipCodeProcessor processor;

    @BeforeEach
    public void setUp() {
        verifier = new Verifier();
        processor = new ZipCodeProcessor(verifier);
    }

    // write your tests here

    @Test
    public void testVerifyTooLongException() {

        InvalidFormatException e = assertThrows(InvalidFormatException.class, () ->
                verifier.verify("9171220"));

        assertEquals("ERRCODE 21: INPUT_TOO_LONG", e.getMessage());
    }

    @Test
    public void testVerifyTooShortException() {
        InvalidFormatException e = assertThrows(InvalidFormatException.class, () ->
                verifier.verify("212"));

        assertEquals("ERRCODE 22: INPUT_TOO_SHORT", e.getMessage());
    }

    @Test
    public void testVerifyNoServiceException() {
        NoServiceException e = assertThrows(NoServiceException.class, () ->
                verifier.verify("11761"));

        assertEquals("ERRCODE 27: NO_SERVICE", e.getMessage());
    }

    @Test
    public void testProcessThankYou() {
        String actual = processor.process("91761");
        String expected = "Thank you!  Your package will arrive soon.";

        assertEquals(expected, actual);
    }
    @Test
    public void testProcessWrongLength(){
        String actual = processor.process("321");
        String expected = "The zip code you entered was the wrong length.";

        assertEquals(expected, actual);

    }
    @Test
    public void testProcessNoService(){
        String actual = processor.process("12345");
        String expected = "We\'re sorry, but the zip code you entered is out of our range.";

        assertEquals(expected, actual);
    }
}